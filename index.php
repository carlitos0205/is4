<?php

require 'vendor/autoload.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$host = 'localhost';
$port = '5432';
$dbname = 'paises';

try {
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error en la conexión a la base de datos: " . $e->getMessage());
}


$app = AppFactory::create();

$app->get('/paises', function (Request $request, Response $response, $args) use ($pdo) {
    
    $query = "SELECT * FROM paises ORDER BY codigoPais";
    $stmt = $pdo->query($query);
    $paises = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $response->getBody()->write(json_encode($paises, JSON_PRETTY_PRINT));
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(200);
});


$app->run();
