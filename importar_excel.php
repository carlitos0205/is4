<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


$host = 'localhost';
$port = '5432';
$dbname = 'paises';



try {
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error en la conexión a la base de datos: " . $e->getMessage());
}


$excelFile = 'paises_estandar.xls';

try {
    $spreadsheet = IOFactory::load($excelFile);
} catch (Exception $e) {
    die("Error al cargar el archivo Excel: " . $e->getMessage());
}


$sheet = $spreadsheet->getActiveSheet();


foreach ($sheet->getRowIterator() as $row) {
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);

    $data = [];
    foreach ($cellIterator as $cell) {
        $data[] = $cell->getValue();
    }

    
    $codigoPais = $data[1];
    $nombrePais = $data[2];

    try {
        $stmt = $pdo->prepare("INSERT INTO paises (codigoPais, nombrePais) VALUES (?, ?)");
        $stmt->execute([$codigoPais, $nombrePais]);
    } catch (PDOException $e) {
        die("Error al insertar datos en la base de datos: " . $e->getMessage());
    }
}

echo "Datos insertados correctamente en la base de datos.";

?>
